import UIKit

struct Paid:IPaid {
    public func getSalary() -> Float {
        return 100
    }
}

public func testPosition() {
    let positionOne = Position.ruleOne
    XCTAssertEqual(positionOne.getRule().calculates(employee: Paid()), 77.5)
    let positionTwo = Position.ruleTwo
    XCTAssertEqual(positionTwo.getRule().calculates(employee: Paid()), 89)
}
testPosition()

public func testEmployee() {
    let name = "Name10"
    let employee:Employee = Employee(id: 10, name: name, salary: 1000, position: Position.ruleOne)
    print("employee \(employee)")
    XCTAssertEqual(employee.getName(), name)
    XCTAssertEqual(employee.calculateSalary(), 775)
}

testEmployee()

//TestHelper(with: [EmployeeTests.self])
