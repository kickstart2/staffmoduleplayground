import Foundation

public protocol IPaid {
    func getSalary() -> Float
}

public protocol CalculationRule {
    func calculates(employee:IPaid) -> Float
}



struct CalculationRuleOne:CalculationRule {
    public func calculates(employee: IPaid) -> Float {
        let salary = employee.getSalary()
        return salary - salary * 0.225
    }
}

struct CalculationRuleTwo:CalculationRule {
    public func calculates(employee: IPaid) -> Float {
        let salary = employee.getSalary()
        return salary - salary * 0.11
    }
}

public enum Position {
    case ruleOne
    case ruleTwo
    
    public func getRule() -> CalculationRule {
        switch self {
        case .ruleOne:
            return CalculationRuleOne()
        case .ruleTwo:
            return CalculationRuleTwo()
        }
    }
}
