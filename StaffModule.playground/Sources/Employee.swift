import Foundation

public class Employee: IPaid {
    private let id:Int
    private let name:String
    private let salary:Float
    private let position:Position
//    private var connection:Connection
    
    public init(id:Int, name:String, salary:Float, position:Position) {
        self.id = id
        self.name = name
        self.salary = salary
        self.position = position
    }
    
    public func getName() -> String {
        return self.name
    }
    
    public func getSalary() -> Float {
        return self.salary
    }
    
    public func calculateSalary() -> Float {
        return self.position.getRule().calculates(employee:self)
    }
    
    public func description() -> String {
        return "Employee [id=\(self.id), name=\(self.name), salary=\(self.salary), position=\(self.position)]"
    }
    
    public func save() throws {
        //store data with connection
    }
}
